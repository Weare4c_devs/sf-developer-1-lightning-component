/**
 * Created by Gunther on 11/03/2019.
 */

public with sharing class SearchMovie {

    @AuraEnabled
    public static List<WebserviceCallout.MovieResponse> searchMovies(String searchTerm){
        return WebserviceCallout.searchMovies(searchTerm);
    }

    private static Id saveMovieToDatabase(List<WebserviceCallout.MovieResponse> theSelectedMovies) {
        List<Movie__c> lstMovies = new List<Movie__c>();
        // map the movieResponse fields to a field in the database, object Movie__c
        for (WebserviceCallout.MovieResponse theSelectedMovie : theSelectedMovies) {
            Movie__c uploadedMovie = new Movie__c(
                    Cover_Url__c = 'https://image.tmdb.org/t/p/original' + theSelectedMovie.poster_path,
                    Name = theSelectedMovie.original_title,
                    Description__c = theSelectedMovie.overview,
                    Release_Date__c = Date.valueOf(theSelectedMovie.release_date),
                    Genre__c = String.join(theSelectedMovie.genres,';')
            );

            lstMovies.add(uploadedMovie);
        }

        try{
            upsert lstMovies;
            return lstMovies[0].Id;
        } catch (DmlException ex){
            throw createAuraHandledException(ex.getDmlMessage(0));
        }
    }

    @AuraEnabled
    public static Id insertTheSelectedMovies(List<WebserviceCallout.MovieResponse> sMovies){
        return saveMovieToDatabase(sMovies);
    }


    private static AuraHandledException createAuraHandledException(String Message){
        AuraHandledException e = new AuraHandledException(Message);
        e.setMessage(Message);
        return e;
    }


}